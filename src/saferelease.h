#ifndef __SAFERELEASE_H__
#define __SAFERELEASE_H__

#ifndef SafeDelete
    #define SafeDelete( _X_ ) { if ( _X_ != NULL ) { delete _X_; _X_ = NULL; } }
#endif /// of SaveDelete

template <class T> void SafeRelease(T **ppT)
{
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = NULL;
    }
}

#endif /// of __SAFERELEASE_H__
