#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include "winMain.h"

#include <wchar.h>

#include <FL/platform.H>
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#if defined(USE_SYSPNG)
    #include <png.h>
#else
    #include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include "winMain.h"
#include "fl_imgtk.h"

#include "resource.h"
#include "dtools.h"
#include "ltools.h"
#include "dxinput.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>


////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "Windows Joystick Tester w/ FLTK"
#define DEF_WIDGET_FSZ          14
#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_WIN_COLOR_BG        0x333333FF
#define DEF_WIN_COLOR_FG        0xAAAAAAFF
#define DEF_COL_INDI            0xFF6633FF
#define DEF_COL_BTN_BG          0x404040FF
#define DEF_COL_BTN_UP_BG       0x605040FF
#define DEF_COL_DET_OK			0x6666FFFF
#define DEF_COL_DET_FAIL		0xFF6666FF

#define APPLY_THM( _w_ )        _w_->color( window->color() );\
                                _w_->labelcolor( window->labelcolor() ); \
                                _w_->labelsize( window->labelsize() ); \
                                _w_->labelcolor( window->labelcolor() );
#define APPLY_THMEX( _w_ )      _w_->color( 0x15151500 ); \
                                _w_->textcolor( DEF_COL_INDI ); \
                                _w_->textfont( FL_COURIER ); \
                                _w_->textsize( window->labelsize() + 2 );
#define APPLY_THMPOV( _w_ )     _w_->box( FL_THIN_DOWN_BOX ); \
                                _w_->color( DEF_COL_BTN_BG ); \
                                _w_->labelfont( FL_COURIER ); \
                                _w_->labelcolor( DEF_COL_INDI );


////////////////////////////////////////////////////////////////////////////////

string resourcebase;

bool getResource( const char* scheme, uchar** buff, unsigned* buffsz )
{
#ifdef _WIN32
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }
#elif defined(__APPLE__)
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }
#ifdef DEBUG
    printf("Failure.\n");
#endif

#else /// --> Linux
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }

#endif /// of _WIN32

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

#ifdef _WIN32
typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    static HMODULE hUser32 = LoadLibraryA( "user32.dll" );
    static MSGBOXWAPI MessageBoxTimeoutW = NULL;
    
    if ( hUser32 != NULL )
    {
        if ( MessageBoxTimeoutW == NULL )
        {
            MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        }
    }
    
    if ( MessageBoxTimeoutW != NULL )
        retI = MessageBoxTimeoutW(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
    else
        retI = MessageBox(hWnd, sText, sCaption, uType);

    return retI;
}
#endif // _WIN32

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv )
 :  _argc( argc ),
    _argv( argv ),
    window( NULL ),
    busyflag( false ),
    keepPolling( false ),
	imgMainBg( NULL ),
	imgStateBg( NULL ),
	imgOverlayBg( NULL )
{
    memset( threads, 0, sizeof( pthread_t ) * MAX_THREADS_CNT );

    getEnvironments();
    createComponents();

#ifdef _WIN32
    HICON \
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    HICON \
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
    }
#endif /// of _WIN32

}

WMain::~WMain()
{
    killThread( THREAD_KILL_ALL );
	
	if ( imgOverlayBg != NULL )
	{
		fl_imgtk::discard_user_rgb_image( imgOverlayBg );
	}
	
	if ( imgStateBg != NULL )
	{
		fl_imgtk::discard_user_rgb_image( imgStateBg );
	}
	
	if ( imgMainBg != NULL )
	{
		fl_imgtk::discard_user_rgb_image( imgMainBg );
	}

}

int WMain::Run()
{
    if ( window != NULL )
    {
        createThread( THREAD_JOB_POLLING );
        return Fl::run();
    }

    return 0;
}

void* WMain::PThreadCall( ThreadParam* tparam )
{
    if ( tparam == NULL )
        return NULL;

    long pLL = tparam->paramL;

    switch ( pLL )
    {
        case THREAD_JOB_POLLING:
            {
                keepPolling = true;
                
                // wait for a second ( while window being shown )
                Sleep( 1000 );
                HWND hWnd = fl_xid( window );
                                                
                bool isXinput = false;

                DWORD retBits = InitDirectInput( hWnd, true );
                
                if ( ( retBits & DXINPUT_INIT_RESULT_JOYSTICK ) == DXINPUT_INIT_RESULT_JOYSTICK )
                {
                    isXinput = true;
                }
                
                if ( isXinput == true )
                {
                    JOYSTICK_OPTIONS joyOpt = {0};
                    GetJoystickOptions( &joyOpt );
                    
                    JOYSTATE joyStat = {0};

                    boxState->label( "X Input joystick detected." );
					boxState->labelcolor( DEF_COL_DET_OK );
                    boxState->redraw();

                    while( keepPolling == true )
                    {
                        if ( UpdateInputStateJoystick() == S_OK )
                        {                           
                            GetJoystickState( &joyStat );
                            
                            Fl::lock();

                            for (unsigned cnt=0; cnt<128; cnt++ )
                            {
                                if ( joyStat.rgbButtons[ cnt ] != 0 )
                                    boxArray[cnt]->color( DEF_COL_BTN_UP_BG );
                                else
                                    boxArray[cnt]->color( DEF_COL_BTN_BG );
                                
                                boxArray[cnt]->redraw();
                            }
                                                        
                            boxPOVU->color( DEF_COL_BTN_BG );
                            boxPOVL->color( DEF_COL_BTN_BG );
                            boxPOVR->color( DEF_COL_BTN_BG );
                            boxPOVD->color( DEF_COL_BTN_BG );
                            
                            unsigned povi = 0;
                            
                            if ( joyStat.rgdwPOV[povi] != 0xFFFFFFFF )
                            {
                                if ( joyStat.rgdwPOV[povi] == 0 )
                                {
                                    boxPOVU->color( DEF_COL_BTN_UP_BG );
                                }
                                
                                if ( joyStat.rgdwPOV[povi] == 0x00002328 )
                                {
                                    boxPOVR->color( DEF_COL_BTN_UP_BG );
                                }
                                
                                if ( joyStat.rgdwPOV[povi] == 0x00004650 )
                                {
                                    boxPOVD->color( DEF_COL_BTN_UP_BG );
                                }
                                
                                if ( joyStat.rgdwPOV[povi] == 0x00006978 )
                                {
                                    boxPOVL->color( DEF_COL_BTN_UP_BG );
                                }
                            }

                            boxPOVU->redraw();
                            boxPOVL->redraw();
                            boxPOVR->redraw();
                            boxPOVD->redraw();
                                                
                            sprintf( strAnalogLX, "%d", joyStat.lX );
                            sprintf( strAnalogLY, "%d", joyStat.lY );
                            sprintf( strAnalogLZ, "%d", joyStat.lZ );

                            sprintf( strAnalogRX, "%d", joyStat.lRx );
                            sprintf( strAnalogRY, "%d", joyStat.lRy );
                            sprintf( strAnalogRZ, "%d", joyStat.lRz );
                                                        
                            outAnalogLX->value( strAnalogLX );
                            outAnalogLY->value( strAnalogLY );
                            outAnalogLZ->value( strAnalogLZ );

                            outAnalogRX->value( strAnalogRX );
                            outAnalogRY->value( strAnalogRY );
                            outAnalogRZ->value( strAnalogRZ );
                                                    
                            Fl::unlock();
                            window->redraw();

                            Fl::awake();
                        }
                        
                        // Window need redraw indirectly.
                        InvalidateRect( hWnd, NULL, TRUE );
                        
                        Sleep( 50 );
                        
                    }

                    FreeDirectInput();
                }
                else
                {
                    boxState->label( "Joystick not detected." );
					boxState->labelcolor( DEF_COL_DET_FAIL );
                    boxState->redraw();
                    
                    // Window need redraw indirectly.
                    InvalidateRect( hWnd, NULL, TRUE );

                }
            }
            break;

        default:
            break;
    }
    
    delete threads[ pLL ];
    threads[ pLL ] = NULL;

    pthread_exit( NULL );

    return NULL;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        if ( keepPolling == true )
        {
            keepPolling = false;
            
            killThread( THREAD_JOB_POLLING );
        }
        return;
    }

}

void WMain::getEnvironments()
{
#ifdef _WIN32
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );

#elif defined(__APPLE__)
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );

        fpos = resourcebase.find_last_of('/');

        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/Resources";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;
#else /// must be Linux
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );
        resourcebase += "/res";
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;

    #ifdef DEBUG
    printf("#DEBUG.PATHES\n");
    printf("HOME: %s\n", pathHome.c_str());
    printf("resourcebase: %s\n",resourcebase.c_str());
    #endif
#endif /// of _WIN32, __APPLE__ and __linux___
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::createComponents()
{
    setdefaultwintitle();

    window = new Fl_Double_Window( 800, 480, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        // continue to child components ...
        window->begin();

            grpMain = new Fl_Group( 0, 0, window->w(), window->h() );
            
            if( grpMain != NULL )
            {
                grpMain->begin();
				
				// Generate gradiation image.
				
				boxMainBg = new Fl_Box( 0, 0, window->w(), window->h() );
				if ( boxMainBg != NULL )
				{
    				imgMainBg = fl_imgtk::makegradation_h( window->w(), window->h(),
														   DEF_WIN_COLOR_BG, 0x33221133,
														   true );
					Fl_RGB_Image* imgPattern = createResImage( "png_patternbg" );
					if ( imgPattern != NULL )
					{
						fl_imgtk::drawonimage( imgMainBg, imgPattern, 0, 0, 0.3f );
						fl_imgtk::discard_user_rgb_image( imgPattern );
					}
					boxMainBg->box( FL_NO_BOX );
					boxMainBg->image( imgMainBg );
				}
                
                boxState = new Fl_Box( 0, 0, 800, 50, "Hold on a second while initializing joystick ..." );
                if ( boxState != NULL )
                {
					// Make blurred back.
					if ( imgMainBg != NULL )
					{
						imgStateBg = fl_imgtk::crop( imgMainBg, 0, 0, 800, 50 );
						if ( imgStateBg != NULL )
						{
							fl_imgtk::gamma_ex( imgStateBg, 0.75 );
							fl_imgtk::blurredimage_ex( imgStateBg, 4 );
							
							boxState->image( imgStateBg );
							boxState->align( FL_ALIGN_IMAGE_BACKDROP );
							boxState->box( FL_FLAT_BOX );
						}
						else
						{
							boxState->box( FL_NO_BOX );
						}
					}
					
                    boxState->labelfont( DEF_WIDGET_FNT );
					boxState->labelcolor( DEF_WIN_COLOR_FG );
                    boxState->labelsize( 20 );
                    
                }
                
                int put_x = 120;
                int put_y = 60;
                
                outAnalogLX = new Fl_Output( put_x, put_y, 100, 30, "Analog Left X :" );
                if ( outAnalogLX != NULL )
                {
                    APPLY_THM(outAnalogLX);
                    APPLY_THMEX(outAnalogLX);
                    put_y += 30 + 5;
                }

                outAnalogLY = new Fl_Output( put_x, put_y, 100, 30, "Analog Left Y :" );
                if ( outAnalogLY != NULL )
                {
                    APPLY_THM(outAnalogLY);
                    APPLY_THMEX(outAnalogLY);
                    put_y += 30 + 5;
                }

                outAnalogLZ = new Fl_Output( put_x, put_y, 100, 30, "Analog Left Z :" );
                if ( outAnalogLZ != NULL )
                {
                    APPLY_THM(outAnalogLZ);
                    APPLY_THMEX(outAnalogLZ);
                    put_y += 30 + 5;
                }
                
                put_x = 340;
                put_y = 60;

                outAnalogRX = new Fl_Output( put_x, put_y, 100, 30, "Analog Right X :" );
                if ( outAnalogRX != NULL )
                {
                    APPLY_THM(outAnalogRX);
                    APPLY_THMEX(outAnalogRX);
                    put_y += 30 + 5;
                }
                
                outAnalogRY = new Fl_Output( put_x, put_y, 100, 30, "Analog Right Y :" );
                if ( outAnalogRY != NULL )
                {
                    APPLY_THM(outAnalogRY);
                    APPLY_THMEX(outAnalogRY);
                    put_y += 30 + 5;
                }

                outAnalogRZ = new Fl_Output( put_x, put_y, 100, 30, "Analog Right Z :" );
                if ( outAnalogRZ != NULL )
                {
                    APPLY_THM(outAnalogRZ);
                    APPLY_THMEX(outAnalogRZ);
                    put_y += 30 + 5;
                }
                
                boxPOVU = new Fl_Box( 500, 60, 50, 30, "UP" );
                if ( boxPOVU != NULL )
                {
                    APPLY_THMPOV(boxPOVU);
                }
                
                boxPOVL = new Fl_Box( 470, 95, 50, 30, "LEFT" );
                if ( boxPOVL != NULL )
                {
                    APPLY_THMPOV(boxPOVL);
                }

                boxPOVR = new Fl_Box( 530, 95, 50, 30, "RIGHT" );
                if ( boxPOVR != NULL )
                {
                    APPLY_THMPOV(boxPOVR);
                }

                boxPOVD = new Fl_Box( 500, 130, 50, 30, "DOWN" );
                if ( boxPOVD != NULL )
                {
                    APPLY_THMPOV(boxPOVD);
                }

				put_y    += 20;
                int btn_l = 25;
                int btn_w = 37;
                int btn_h = 30;
                put_x     = btn_l;
                                
                for( unsigned cnt=0; cnt<128; cnt++ )
                {
                    char strTmp[32] = {0};                  
                    sprintf( strTmp, "%03u", cnt );
                    strBtnsLbl.push_back( strTmp );
                                        
                    Fl_Box* boxTmp = new Fl_Box( put_x, put_y, btn_w, btn_h );
                    if ( boxTmp != NULL )
                    {
                        APPLY_THMPOV(boxTmp);
                        
                        boxArray.push_back( boxTmp );
                        
                        put_x += btn_w + 5;
                        
                        if ( ( put_x + btn_w + 5 ) > ( grpMain->w() - 5 ) )
                        {
                            put_x = btn_l;
                            put_y += btn_h + 5;
                        }

                    }                                   
                }   
                
                for( unsigned cnt=0; cnt<128; cnt++ )
                {
                    boxArray[cnt]->label( strBtnsLbl[cnt].c_str() );
                }
				
				int cpr_w = ( grpMain->w() / 2 ) - 10 ;
				int cpr_h = btn_h;
				int cpr_x = grpMain->w() - cpr_w - 5;
				int cpr_y = grpMain->h() - cpr_h - 5;
				
				boxCopyright = new Fl_Box( cpr_x, cpr_y, cpr_w, cpr_h,
										   "(C)2018 Raphael Kim ( rageworx@@gmail.com )" );
				if ( boxCopyright != NULL )
				{
					APPLY_THM(boxCopyright);
					boxCopyright->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT );
				}

                grpMain->end();
            }
        
            grpOverlay = new Fl_Group( 0, 0, window->w(), window->h() );
            if ( grpOverlay != NULL )
            {
                grpOverlay->begin();
                
                boxOverlayBg = new Fl_Box( 0,0, grpOverlay->w(), grpOverlay->h() );
                if ( boxOverlayBg != NULL )
                {
                    boxOverlayBg->box( FL_FLAT_BOX );
                }
                
                grpOverlay->end();
                grpOverlay->hide();
            }
        

        window->end();
        window->show();
    }
}

bool WMain::createThread( unsigned idx )
{
    if ( idx < MAX_THREADS_CNT )
    {
        killThread( idx );

        threads[idx] = new ThreadParam;
        if ( threads[idx] != NULL )
        {
            threads[idx]->paramL = idx;
            threads[idx]->paramData = this;

            pthread_create( &threads[idx]->ptt,
                            NULL, WMain::PThreadCB,
                            threads[idx] );

            return true;
        }
    }
    return false;
}

void WMain::killThread( unsigned idx )
{
    if ( idx == THREAD_KILL_ALL )
    {
        for( unsigned cnt=0; cnt<MAX_THREADS_CNT; cnt++ )
        {
            killThread( cnt );
        }

        return;
    }
    else
    if ( idx < MAX_THREADS_CNT )
    {
        if ( threads[idx] != NULL )
        {
            pthread_t ptt = threads[idx]->ptt;
            if ( ptt > 0 )
            {
                pthread_kill( ptt, 0 );
                pthread_join( ptt, NULL );
            }

            delete threads[idx];
            threads[idx] = NULL;
        }
    }
}
