#ifdef _WIN32
    #include <windows.h>
#endif /// of _WIN32

#include <unistd.h>

#include "dtools.h"

// Godam IO headers !
#ifdef __APPLE__
    #include <sys/uio.h>
    #include <sys/types.h>
    #include <sys/stat.h>
#elif defined(_WIN32)
    #include <io.h>
#else
    #include <sys/io.h>
    #include <sys/types.h>
    #include <sys/stat.h>
#endif /// of __APPLE__

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iostream>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <iterator>

#include <dirent.h>

#include "ltools.h"

////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
    #define DEF_DIR_SEP_W       L"\\"
    #define DEF_DIR_SEP_M       "\\"

#else
    #define DEF_DIR_SEP_W       L"/"
    #define DEF_DIR_SEP_M       "/"
#endif /// of _WIN32

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

const wchar_t* platformdirseperator()
{
    return DEF_DIR_SEP_W;
}

bool testDirectory( const wchar_t* path )
{
    if ( path != NULL )
    {
        _WDIR* dir = NULL;

        dir = _wopendir( path );

        if ( dir != NULL )
        {
            _wclosedir( dir );
            return true;
        }
    }

    return false;
}

bool createDirectory( const wchar_t* path )
{
    if ( path != NULL )
    {
        if( _wmkdir( path ) == 0 )
            return true;
    }

    return false;
}

bool copyFile( const wchar_t* src, const wchar_t* dst )
{
    if ( ( src != NULL ) && ( dst != NULL ) )
    {
        if ( ::CopyFile( src, dst, FALSE ) == TRUE )
            return true;
    }

    return false;
}

unsigned searchFiles( const wchar_t* fpath, vector<wstring> &files, const wchar_t* ext, bool recursive )
{
    if ( fpath == NULL )
        return 0;

    _WDIR* dir      = NULL;
    struct \
    _wdirent* dire  = NULL;

    dir = _wopendir( fpath );

    if ( dir != NULL )
    {
        while ( ( dire = _wreaddir( dir ) ) )
        {
            wstring pathasm = dire->d_name;

            if ( ( pathasm != L"." ) && ( pathasm != L".." ) )
            {
                if ( recursive == true )
                {
                    // check current name is a file or directory ...
                    wstring pathtest = fpath;
                    pathtest += DEF_DIR_SEP_W;
                    pathtest += pathasm;

                    _WDIR* dirtest = _wopendir( pathtest.c_str() );
                    if ( dirtest != NULL )
                    {
                        vector<wstring> tmplist;
                        searchFiles( pathtest.c_str(),
                                     tmplist,
                                     ext,
                                     true );

                        if ( tmplist.size() > 0 )
                        {
                            // make sub-directory path before file name ...
                            wstring tmpasm = pathasm;
                            tmpasm += DEF_DIR_SEP_W;

                            for( size_t cnt=0; cnt<tmplist.size(); cnt++ )
                            {
                                wstring tmpitem = tmpasm;
                                wstring fndname = tmplist[ cnt ];
                                tmpitem += fndname;

                                if ( ext != NULL )
                                {
                                    wstring strext = extractFileExtension( fndname.c_str() );
                                    if ( strext == ext )
                                    {
                                        files.push_back( tmpitem );
                                    }
                                }
                                else
                                {
                                    files.push_back( tmpitem );
                                }
                            }
                        }

                        _wclosedir( dirtest );
                    }
                    else
                    {
                        if ( ext != NULL )
                        {
                            wstring strext = extractFileExtension( pathasm.c_str() );
                            if ( strext == ext )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                }
                else
                {
                    _WDIR* dirtest = _wopendir( pathasm.c_str() );
                    if ( dirtest == NULL )
                    {
                        if ( ext != NULL )
                        {
                            if ( pathasm.find_last_of( ext ) != wstring::npos )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                    else
                    {
                        _wclosedir( dirtest );
                    }
                }

            }
        }

        _wclosedir(dir);
    }

    return (unsigned)files.size();
}

bool removeFilesInDir( const wchar_t* fpath, const wchar_t* ext )
{
    if ( fpath != NULL )
    {
        vector<wstring> files;

        unsigned retsz = searchFiles( fpath, files, ext );
        if ( retsz > 0 )
        {
            unsigned failurecnt = 0;

            #pragma omp parallel for
            for( unsigned cnt=0; cnt<retsz; cnt++ )
            {
                wchar_t tmpnm[512] = {0,};

                wsprintf( tmpnm, L"%S%S%S",
                          fpath,
                          DEF_DIR_SEP_W,
                          files[cnt].c_str() );

                if ( _waccess( tmpnm, F_OK ) == 0 )
                {
                    if ( _wunlink( tmpnm ) != 0 )
                    {
                        failurecnt++;
                    }
                }
            }

            files.clear();

            if ( failurecnt > 0 )
                return false;

            return true;
        }

        // If nothing to erase, just return true.
        printf("(nothing)");
        return true;
    }

    return false;
}

void makeDirPlatformize( wstring &path )
{
    if ( path.size() > 0 )
    {
        const wchar_t* dirSep = DEF_DIR_SEP_W;

        size_t fpos = path.find( dirSep );
        while( fpos != string::npos )
        {
            path.replace( fpos, 1, dirSep );
            fpos = path.find( dirSep, fpos );
        }
    }
}

void removeLastDirSep( std::wstring &path )
{
    if ( path.size() > 1 )
    {
        const wchar_t* ppath = path.c_str();
        const wchar_t  dirsep[] = DEF_DIR_SEP_W;

        if ( ppath[ path.size() - 1 ] == dirsep[0] )
        {
            path.erase( path.size() - 1 );
        }
    }
}

const wchar_t* stripFilePath( const wchar_t* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static wstring fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( 0, last_path_div_pos );
    }

    return fpath.c_str();
}

const wchar_t* stripFileName( const wchar_t* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static wstring fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( last_path_div_pos + 1 );
    }

    return fpath.c_str();
}

const wchar_t* removeFilePath( const wchar_t* file_path )
{
    if ( file_path == NULL )
        return NULL;

    static wstring fname;

    fname = file_path;

    size_t last_path_div_pos = fname.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fname = fname.substr( last_path_div_pos + 1 );
    }

    return fname.c_str();
}

bool testDirectory( const char* path )
{
    if ( path != NULL )
    {
        DIR* dir = NULL;

        dir = opendir( path );

        if ( dir != NULL )
        {
            closedir( dir );
            return true;
        }
    }

    return false;
}

bool createDirectory( const char* path )
{
    if ( path != NULL )
    {
#ifdef _WIN32
        if( mkdir( path ) == 0 )
#else
        if( mkdir( path, 0 ) == 0 )
#endif // _WIN32
            return true;
    }

    return false;
}

bool copyFile( const char* src, const char* dst )
{
    if ( ( src != NULL ) && ( dst != NULL ) )
    {
        ifstream f1 ( src, fstream::binary );
        ofstream f2 ( dst, fstream::trunc|fstream::binary );
        f2 << f1.rdbuf ();
        f1.close();
        f2.close();

		return true;
    }

    return false;
}

unsigned searchFiles( const char* fpath, vector<string> &files, const char* ext, bool recursive )
{
    if ( fpath == NULL )
        return 0;

    DIR* dir      = NULL;
    struct \
    dirent* dire  = NULL;

    dir = opendir( fpath );

    if ( dir != NULL )
    {
        while ( ( dire = readdir( dir ) ) )
        {
            string pathasm = dire->d_name;

            if ( ( pathasm != "." ) && ( pathasm != ".." ) )
            {
                if ( recursive == true )
                {
                    // check current name is a file or directory ...
                    string pathtest = fpath;
                    pathtest += DEF_DIR_SEP_M;
                    pathtest += pathasm;

                    DIR* dirtest = opendir( pathtest.c_str() );
                    if ( dirtest != NULL )
                    {
                        vector<string> tmplist;
                        searchFiles( pathtest.c_str(),
                                     tmplist,
                                     ext,
                                     true );

                        if ( tmplist.size() > 0 )
                        {
                            // make sub-directory path before file name ...
                            string tmpasm = pathasm;
                            tmpasm += DEF_DIR_SEP_M;

                            for( size_t cnt=0; cnt<tmplist.size(); cnt++ )
                            {
                                string tmpitem = tmpasm;
                                string fndname = tmplist[ cnt ];
                                tmpitem += fndname;

                                if ( ext != NULL )
                                {
                                    string strext = extractFileExtension( fndname.c_str() );
                                    if ( strext == ext )
                                    {
                                        files.push_back( tmpitem );
                                    }
                                }
                                else
                                {
                                    files.push_back( tmpitem );
                                }
                            }
                        }

                        closedir( dirtest );
                    }
                    else
                    {
                        if ( ext != NULL )
                        {
                            string strext = extractFileExtension( pathasm.c_str() );
                            if ( strext == ext )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                }
                else
                {
                    DIR* dirtest = opendir( pathasm.c_str() );
                    if ( dirtest == NULL )
                    {
                        if ( ext != NULL )
                        {
                            if ( pathasm.find_last_of( ext ) != string::npos )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                    else
                    {
                        closedir( dirtest );
                    }
                }

            }
        }

        closedir(dir);
    }

    return (unsigned)files.size();
}

bool removeFilesInDir( const char* fpath, const char* ext )
{
    if ( fpath != NULL )
    {
        vector<string> files;

        unsigned retsz = searchFiles( fpath, files, ext );
        if ( retsz > 0 )
        {
            unsigned failurecnt = 0;

            #pragma omp parallel for
            for( unsigned cnt=0; cnt<retsz; cnt++ )
            {
                char tmpnm[512] = {0,};

                sprintf( tmpnm, "%s%s%s",
                         fpath,
                         DEF_DIR_SEP_M,
                         files[cnt].c_str() );

                if ( access( tmpnm, F_OK ) == 0 )
                {
                    if ( unlink( tmpnm ) != 0 )
                    {
                        failurecnt++;
                    }
                }
            }

            files.clear();

            if ( failurecnt > 0 )
                return false;

            return true;
        }

        // If nothing to erase, just return true.
        return true;
    }

    return false;
}

void makeDirPlatformize( string &path )
{
    if ( path.size() > 0 )
    {
        const char* dirSep = DEF_DIR_SEP_M;

        size_t fpos = path.find( dirSep );
        while( fpos != string::npos )
        {
            path.replace( fpos, 1, dirSep );
            fpos = path.find( dirSep, fpos );
        }
    }
}

void removeLastDirSep( std::string &path )
{
    if ( path.size() > 1 )
    {
        const char* ppath = path.c_str();
        const char dirsep[] = DEF_DIR_SEP_M;

        if ( ppath[ path.size() - 1 ] == dirsep[0] )
        {
            path.erase( path.size() - 1 );
        }
    }
}

const char* stripFilePath( const char* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static string fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_M );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( 0, last_path_div_pos );
    }

    return fpath.c_str();
}

const char* stripFileName( const char* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static string fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_M );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( last_path_div_pos + 1 );
    }

    return fpath.c_str();
}

const char* removeFilePath( const char* file_path )
{
    if ( file_path == NULL )
        return NULL;

    static string fname;

    fname = file_path;

    size_t last_path_div_pos = fname.find_last_of( DEF_DIR_SEP_M );

    if ( last_path_div_pos != string::npos )
    {
        fname = fname.substr( last_path_div_pos + 1 );
    }

    return fname.c_str();
}
