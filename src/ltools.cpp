#include "ltools.h"

#include <string>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////


const wchar_t* extractFileExtension( const wchar_t* file_name )
{
    if ( file_name == NULL )
        return NULL;

    static wstring fnameext;

    fnameext = file_name;

    size_t fname_len = fnameext.size();
    size_t extpos = fnameext.find_last_of( L'.' );

    if ( extpos != wstring::npos )
    {
        fnameext = fnameext.substr( extpos );
    }

    return fnameext.c_str();
}

const wchar_t* extractLastPath( const wchar_t* path )
{
    if ( path == NULL )
        return NULL;

    static wstring retname;
    retname = path;

    const wchar_t dirsep = L'\\';

    size_t fname_len = retname.size();
    size_t extpos = retname.find_last_of( dirsep );

    if ( extpos == fname_len )
    {
        retname = retname.substr( 0, extpos - 1 );
        extpos = retname.find_last_of( dirsep );
    }

    if ( extpos != string::npos )
    {
        retname = retname.substr( extpos + 1 );
    }

    return retname.c_str();
}

bool stringCompare( const wstring &left, const wstring &right )
{
    for( wstring::const_iterator lit = left.begin(), rit = right.begin(); lit != left.end() && rit != right.end(); ++lit, ++rit )
    {
        if( tolower( *lit ) < tolower( *rit ) )
        {
            return true;
        }
        else
        if( tolower( *lit ) > tolower( *rit ) )
        {
            return false;
        }
    }

    if( left.size() < right.size() )
        return true;

    return false;
}

vector<wstring> split(const wstring& s, wchar_t seperator)
{
    vector<wstring> output;
    string::size_type prev_pos = 0;
    string::size_type pos = 0;

    while( ( pos = s.find(seperator, pos)) != wstring::npos )
    {
        wstring substring( s.substr(prev_pos, pos-prev_pos) );

        output.push_back(substring);

        prev_pos = ++pos;
    }

    output.push_back( s.substr( prev_pos, pos-prev_pos ) ); // Last word

    return output;
}

unsigned findstring( std::vector<std::wstring> &src, std::wstring findstr )
{
    for( int cnt=0; cnt<src.size(); cnt++ )
    {
        if ( src[cnt] == findstr )
            return cnt;
    }

    return 0;
}

const char* extractFileExtension( const char* file_name )
{
    if ( file_name == NULL )
        return NULL;

    static string fnameext;

    fnameext = file_name;

    size_t fname_len = fnameext.size();
    size_t extpos = fnameext.find_last_of( '.' );

    if ( extpos != string::npos )
    {
        fnameext = fnameext.substr( extpos );
    }

    return fnameext.c_str();
}

const char* extractLastPath( const char* path )
{
    if ( path == NULL )
        return NULL;

    static string retname;
    retname = path;

    const char dirsep = '/';;

    size_t fname_len = retname.size();
    size_t extpos = retname.find_last_of( dirsep );

    if ( extpos == fname_len )
    {
        retname = retname.substr( 0, extpos - 1 );
        extpos = retname.find_last_of( dirsep );
    }

    if ( extpos != string::npos )
    {
        retname = retname.substr( extpos + 1 );
    }

    return retname.c_str();
}

bool stringCompare( const string &left, const string &right )
{
    for( string::const_iterator lit = left.begin(), rit = right.begin(); lit != left.end() && rit != right.end(); ++lit, ++rit )
    {
        if( tolower( *lit ) < tolower( *rit ) )
        {
            return true;
        }
        else
        if( tolower( *lit ) > tolower( *rit ) )
        {
            return false;
        }
    }

    if( left.size() < right.size() )
        return true;

    return false;
}

vector<string> split(const string& s, char seperator)
{
    vector<string> output;
    string::size_type prev_pos = 0;
    string::size_type pos = 0;

    while( ( pos = s.find(seperator, pos)) != wstring::npos )
    {
        string substring( s.substr(prev_pos, pos-prev_pos) );

        output.push_back(substring);

        prev_pos = ++pos;
    }

    output.push_back( s.substr( prev_pos, pos-prev_pos ) ); // Last word

    return output;
}

unsigned findstring( std::vector<std::string> &src, std::string findstr )
{
    for( int cnt=0; cnt<src.size(); cnt++ )
    {
        if ( src[cnt] == findstr )
            return cnt;
    }

    return 0;
}
