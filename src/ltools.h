#ifndef __LTOOLS_H__
#define __LTOOLS_H__

// multiform LTOOLS

#include <vector>
#include <string>

    const wchar_t* extractFileExtension( const wchar_t* file_name );
    const wchar_t* extractLastPath( const wchar_t* path );
    const wchar_t* removeFileExtension( const wchar_t* file_name );
    bool stringCompare( const std::wstring &left, const std::wstring &right );
    std::vector<std::wstring> split(const std::wstring& s, wchar_t seperator);
    unsigned findstring( std::vector<std::wstring> &src, std::wstring findstr );

    const char* extractFileExtension( const char* file_name );
    const char* extractLastPath( const char* path );
    const char* removeFileExtension( const char* file_name );
    bool stringCompare( const std::string &left, const std::string &right );
    std::vector<std::string> split(const std::string& s, char seperator);
    unsigned findstring( std::vector<std::string> &src, std::string findstr );

#endif // __LTOOLS_H__
