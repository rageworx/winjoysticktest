// =============================================================================
// DirectX input for MinGW-W64
// -----------------------------------------------------------------------------
// (C)2015,2016, Programmed Raphael Kim (rage.kim@gmail.com)
// Code refered to MSDN old repository.
// =============================================================================
#ifndef __DXINPUT_H__
#define __DXINPUT_H__

#ifndef _WIN32_DCOM
    #define _WIN32_DCOM
#endif

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <commctrl.h>
#include <basetsd.h>

// ------------------------------------------
// dinput.h included in same source position.
// dinput.h was imported from JEDI project.
#include "dinput.h"
// ------------------------------------------

#include <dinput.h>
#include "dinputd.h"
#include <assert.h>
#include <oleauto.h>
#include <shellapi.h>

// Stuff to filter out XInput devices
#include <wbemidl.h>

#define DXINPUT_INIT_RESULT_NONE        0   /// 0000
#define DXINPUT_INIT_RESULT_KEYBOARD    1   /// 0001
#define DXINPUT_INIT_RESULT_MOUSE       2   /// 0010
#define DXINPUT_INIT_RESULT_JOYSTICK    4   /// 0100
#define DXINPUT_INIT_RESULT_ALL         7   /// 0111

#define XJOYSTICK_MAX_SLIDERS       32
#define XJOYSTICK_MAX_POVS          32
#define XJOYSTICK_MAX_BUTTONS       32
#define XJOYSTICK_MAX_KEYS          128

struct XINPUT_DEVICE_NODE
{
    DWORD dwVidPid;
    XINPUT_DEVICE_NODE* pNext;
};

struct DI_ENUM_CONTEXT
{
    DIJOYCONFIG* pPreferredJoyCfg;
    bool bPreferredJoyCfgValid;
};

struct JOYSTICK_OPTIONS
{
    bool Axis_X;
    bool Axis_Y;
    bool Axis_Z;
    bool Axis_X_ROT;
    bool Axis_Y_ROT;
    bool Axis_Z_ROT;
    int  Sliders;
    bool Slider[XJOYSTICK_MAX_SLIDERS];
    int  POVs;
    bool POV[XJOYSTICK_MAX_POVS];
    int  Buttons;
    bool Button[XJOYSTICK_MAX_BUTTONS];
    int  Keys;
    bool Key[XJOYSTICK_MAX_KEYS];
};

typedef struct{
    LONG    lX;                     /* x-axis position              */
    LONG    lY;                     /* y-axis position              */
    LONG    lZ;                     /* z-axis position              */
    LONG    lRx;                    /* x-axis rotation              */
    LONG    lRy;                    /* y-axis rotation              */
    LONG    lRz;                    /* z-axis rotation              */
    LONG    rglSlider[2];           /* extra axes positions         */
    DWORD   rgdwPOV[4];             /* POV directions               */
    BYTE    rgbButtons[128];        /* 128 buttons                  */
    LONG    lVX;                    /* x-axis velocity              */
    LONG    lVY;                    /* y-axis velocity              */
    LONG    lVZ;                    /* z-axis velocity              */
    LONG    lVRx;                   /* x-axis angular velocity      */
    LONG    lVRy;                   /* y-axis angular velocity      */
    LONG    lVRz;                   /* z-axis angular velocity      */
    LONG    rglVSlider[2];          /* extra axes velocities        */
    LONG    lAX;                    /* x-axis acceleration          */
    LONG    lAY;                    /* y-axis acceleration          */
    LONG    lAZ;                    /* z-axis acceleration          */
    LONG    lARx;                   /* x-axis angular acceleration  */
    LONG    lARy;                   /* y-axis angular acceleration  */
    LONG    lARz;                   /* z-axis angular acceleration  */
    LONG    rglASlider[2];          /* extra axes accelerations     */
    LONG    lFX;                    /* x-axis force                 */
    LONG    lFY;                    /* y-axis force                 */
    LONG    lFZ;                    /* z-axis force                 */
    LONG    lFRx;                   /* x-axis torque                */
    LONG    lFRy;                   /* y-axis torque                */
    LONG    lFRz;                   /* z-axis torque                */
    LONG    rglFSlider[2];          /* extra axes forces            */
} JOYSTATE;

typedef struct {
  LONG lX;
  LONG lY;
  LONG lZ;
  BYTE buttons[8];
} MOUSESTATE;

typedef struct{
    UINT  length;
    UCHAR keys[256];
} KEYSTATE;

DWORD       InitDirectInput( HWND hDlg, bool isxinput );
void        FreeDirectInput();
HRESULT     UpdateInputStateJoystick();
HRESULT     UpdateInputStateKeyboard();
HRESULT     UpdateInputStateMouse();
void        GetJoystickOptions( JOYSTICK_OPTIONS* jopt );
void        GetJoystickState( JOYSTATE* jst );
void        GetKeyboardState( KEYSTATE* kst );
void        GetMouseState( MOUSESTATE* mst );
HRESULT     GetDXInputLastResult();

#endif /// of __DXINPUT_H__
