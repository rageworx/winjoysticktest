#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Output.H>

#include <pthread.h>

#include <vector>
#include <string>

class WMain
{
    typedef struct
        _ThreadParam{
            pthread_t   ptt;
            long        paramL;
            void*       paramData;
        }ThreadParam;

    public:
        WMain( int argc, char** argv );
        ~WMain();

    public:
        int Run();

    public:
        void* PThreadCall( ThreadParam* tparam );
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                ThreadParam* tp = (ThreadParam*)p;
                WMain* pwm = (WMain*)tp->paramData;
                return pwm->PThreadCall(tp);
            }
            return NULL;
        }
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }

    private:
        void setdefaultwintitle();
        void createComponents();
        void getEnvironments();
        bool createThread( unsigned idx );
        void killThread( unsigned idx );

    private:
        int     _argc;
        char**  _argv;

    private:
        bool        busyflag;

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpMain;
		Fl_Box*				boxMainBg;
        Fl_Box*             boxState;
        Fl_Output*          outAnalogLX;
        Fl_Output*          outAnalogLY;
        Fl_Output*          outAnalogLZ;
        Fl_Output*          outAnalogRX;
        Fl_Output*          outAnalogRY;
        Fl_Output*          outAnalogRZ;
        std::vector<Fl_Box*> \
                            boxArray;
        Fl_Box*             boxPOVL;
        Fl_Box*             boxPOVR;
        Fl_Box*             boxPOVU;
        Fl_Box*             boxPOVD;
		Fl_Box*				boxCopyright;
		
        Fl_Group*           grpOverlay;
        Fl_Box*             boxOverlayBg;
		Fl_RGB_Image*		imgMainBg;
		Fl_RGB_Image*		imgStateBg;
		Fl_RGB_Image*		imgOverlayBg;
		
    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:  
        bool                keepPolling;
        char                strAnalogLX[80];
        char                strAnalogLY[80];
        char                strAnalogLZ[80];
        char                strAnalogRX[80];
        char                strAnalogRY[80];
        char                strAnalogRZ[80];
        std::vector<std::string>\
                            strBtnsLbl;
        
    protected:
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;

    protected:
        std::wstring        pathImage;
        std::wstring        imgFName;
        std::string         imgFNameUTF8;

    protected:
        #define             MAX_THREADS_CNT             2
        #define             THREAD_KILL_ALL             -1
        #define             THREAD_JOB_POLLING          1
        ThreadParam*        threads[MAX_THREADS_CNT];

};

#endif // __WINMAIN_H__
