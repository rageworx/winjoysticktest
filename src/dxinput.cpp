#include "dxinput.h"
#include "saferelease.h"

////////////////////////////////////////////////////////////////////////////////

#define MAX_XINPUT_DEVICES  20
#define SAFE_DELETE(p)      { if(p) { delete (p);     (p)=NULL; } }

////////////////////////////////////////////////////////////////////////////////

LPDIRECTINPUT8          pDxInput = NULL;
LPDIRECTINPUTDEVICE8    pDxInpJoy = NULL;
LPDIRECTINPUTDEVICE8    pDxInpKbd = NULL;
LPDIRECTINPUTDEVICE8    pDxInpMse = NULL;
bool                    isDxInpXmode = false;
XINPUT_DEVICE_NODE*     pDxInpXmodeDevList = NULL;
DIJOYSTATE2             DxJoyState;
UCHAR                   DxKbdState[256];
DIMOUSESTATE2           DxMseState;
JOYSTICK_OPTIONS        DxJoyOption = {0};

HRESULT                 lastHR = S_OK;

////////////////////////////////////////////////////////////////////////////////

HRESULT SetupXInputDevice( bool needCOMinit = false );
bool    IsXInputDevice( const GUID* pGuidProductFromDirectInput );
void    CleanupXInputDevice();

////////////////////////////////////////////////////////////////////////////////

BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext );
BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance, VOID* pContext );

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD InitDirectInput( HWND hDlg, bool isxinput )
{
    DWORD initBits = DXINPUT_INIT_RESULT_NONE;

    lastHR = S_FALSE;

    lastHR = DirectInput8Create( GetModuleHandle( NULL ),
                                 DIRECTINPUT_VERSION,
                                 IID_IDirectInput8,
                                 (VOID**)&pDxInput,
                                 NULL );

    if( FAILED( lastHR ) )
    {
        return initBits;
    }

    isDxInpXmode = isxinput;

    // -------------------------------------------------------------------------
    // Keyboard
    if ( pDxInpKbd == NULL )
    {
        lastHR = pDxInput->CreateDevice( GUID_SysKeyboard, &pDxInpKbd, NULL );

        if( FAILED( lastHR ) )
        {
            return initBits;
        }

        // DirectX 10, or above need set this sequence to valid parameter.
        // Let set keyboard for standard input.
        pDxInpKbd->SetDataFormat( &c_dfDIKeyboard );

        lastHR = pDxInpKbd->SetCooperativeLevel( hDlg, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE );
        if ( FAILED( lastHR ) )
        {
            return initBits;
        }

        lastHR = pDxInpKbd->Acquire();
#ifdef DEBUG
        if ( ( lastHR != DI_OK ) && ( lastHR != DI_NOEFFECT ) )
        {
            printf("Error: Dxinput keyboard init-acquire error : 0x%08X\n", lastHR );

            switch( lastHR )
            {
                case DIERR_NOTINITIALIZED:
                    printf("\tNot initialized.\n");
                    break;

                case DIERR_INVALIDPARAM:
                    printf("\tInvalid parameter.\n");
                    break;

                case DIERR_OTHERAPPHASPRIO:
                    printf("\tOther app has priority.\n");
                    break;

                default:
                    printf("\tUnknwon...\n");
                    break;
            }
        }
#endif
        initBits |= DXINPUT_INIT_RESULT_KEYBOARD;
    }

    // -------------------------------------------------------------------------
    // Mouse
    if ( pDxInpMse == NULL )
    {
        lastHR = pDxInput->CreateDevice( GUID_SysMouse, &pDxInpMse, NULL );

        if ( FAILED( lastHR ) )
        {
            return initBits;
        }

        pDxInpMse->SetDataFormat( &c_dfDIMouse2 );

        lastHR = pDxInpMse->SetCooperativeLevel( hDlg, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND );

        if ( FAILED( lastHR ) )
        {
            return initBits;
        }

        lastHR = pDxInpMse->Acquire();
#ifdef DEBUG
        if ( ( lastHR != DI_OK ) && ( lastHR != DI_NOEFFECT ) )
        {
            printf("Error: Dxinput mouse init-acquire error : 0x%08X\n", lastHR );

            switch( lastHR )
            {
                case DIERR_NOTINITIALIZED:
                    printf("\tNot initialized.\n");
                    break;

                case DIERR_INVALIDPARAM:
                    printf("\tInvalid parameter.\n");
                    break;

                case DIERR_OTHERAPPHASPRIO:
                    printf("\tOther app has priority.\n");
                    break;

                default:
                    printf("\tUnknwon...\n");
                    break;
            }
        }
#endif

        initBits |= DXINPUT_INIT_RESULT_MOUSE;
    }


    // -------------------------------------------------------------------------
    // It's turn to joystick.
    if ( pDxInpJoy == NULL )
    {
        if( isDxInpXmode == true )
        {
            SetupXInputDevice();
        }

        DIJOYCONFIG PreferredJoyCfg = {0};
        DI_ENUM_CONTEXT enumContext;
        enumContext.pPreferredJoyCfg = &PreferredJoyCfg;
        enumContext.bPreferredJoyCfgValid = false;

        IDirectInputJoyConfig8* pJoyConfig = NULL;

        lastHR = pDxInput->QueryInterface( IID_IDirectInputJoyConfig8, ( void** )&pJoyConfig );

        if( FAILED( lastHR ) )
            return initBits;

        //test joystick ...
        PreferredJoyCfg.dwSize = sizeof( PreferredJoyCfg );

        // This function is expected to fail if no joystick is attached
        if( SUCCEEDED( pJoyConfig->GetConfig( 0, &PreferredJoyCfg, DIJC_GUIDINSTANCE ) ) )
        {
            enumContext.bPreferredJoyCfgValid = true;
        }
        else
        {
            return initBits;
        }

        SafeRelease( &pJoyConfig );

        // Look for a simple joystick we can use for this sample program.
        lastHR = pDxInput->EnumDevices( DI8DEVCLASS_GAMECTRL,
                                        EnumJoysticksCallback,
                                        &enumContext, DIEDFL_ATTACHEDONLY );
        if( FAILED( lastHR ) )
            return initBits;

        if( isDxInpXmode == true )
        {
            CleanupXInputDevice();
        }

        // Let check pDxInpJoy exists.
        // Sometimes, Xinput mode not exists, so it need to be check pointer.
        if ( pDxInpJoy == NULL )
        {
            return initBits;
        }

        lastHR = pDxInpJoy->SetDataFormat( &c_dfDIJoystick2 );

        if( FAILED( lastHR ) )
            return initBits;

        lastHR = pDxInpJoy->SetCooperativeLevel( hDlg, DISCL_EXCLUSIVE | DISCL_FOREGROUND );

        if( FAILED( lastHR ) )
            return initBits;


        lastHR = pDxInpJoy->EnumObjects( EnumObjectsCallback, (void*)hDlg, DIDFT_ALL );

        if( FAILED( lastHR ) )
            return initBits;

        initBits |= DXINPUT_INIT_RESULT_JOYSTICK;
    }

    return initBits;
}

HRESULT SetupXInputDevice( bool needCOMinit )
{
    IWbemServices*          pIWbemServices = NULL;
    IEnumWbemClassObject*   pEnumDevices = NULL;
    IWbemLocator*           pIWbemLocator = NULL;
    IWbemClassObject*       pDevices[MAX_XINPUT_DEVICES] = {0};

    BSTR                    bstrDeviceID = NULL;
    BSTR                    bstrClassName = NULL;
    BSTR                    bstrNamespace = NULL;

    bool                    bCleanupCOM = false;

    UINT                    iDevice   = 0;
    DWORD                   uReturned = 0;
    VARIANT                 var;

    // Don't do it if already initialized in dxgraph...
    if ( needCOMinit == true )
    {
        lastHR = CoInitialize( NULL );

        if ( FAILED( lastHR ) )
        {
            return lastHR;
        }

        bCleanupCOM = SUCCEEDED( lastHR );
    }

    // Create WMI
    lastHR = CoCreateInstance( __uuidof( WbemLocator ),
                               NULL,
                               CLSCTX_INPROC_SERVER,
                               __uuidof( IWbemLocator ),
                              (LPVOID*)&pIWbemLocator );

    if( FAILED( lastHR ) || pIWbemLocator == NULL )
        goto ExitSetupXinput;

    // Create BSTRs for WMI
    bstrNamespace = SysAllocString( TEXT("\\\\.\\root\\cimv2") );
    if( bstrNamespace == NULL )
        goto ExitSetupXinput;

    bstrDeviceID  = SysAllocString( TEXT("DeviceID") );
    if( bstrDeviceID == NULL )
        goto ExitSetupXinput;

    bstrClassName = SysAllocString( TEXT("Win32_PNPEntity") );
    if( bstrClassName == NULL )
        goto ExitSetupXinput;

    // Connect to WMI
    lastHR = pIWbemLocator->ConnectServer( bstrNamespace,
                                           NULL,
                                           NULL,
                                           0L,
                                           0L,
                                           NULL,
                                           NULL,
                                           &pIWbemServices );

    if( FAILED( lastHR ) || ( pIWbemServices == NULL ) )
        goto ExitSetupXinput;

    // Switch security level to IMPERSONATE
    CoSetProxyBlanket( pIWbemServices,
                       RPC_C_AUTHN_WINNT,
                       RPC_C_AUTHZ_NONE,
                       NULL,
                       RPC_C_AUTHN_LEVEL_CALL,
                       RPC_C_IMP_LEVEL_IMPERSONATE,
                       NULL,
                       0 );

    // Get list of Win32_PNPEntity devices
    lastHR = pIWbemServices->CreateInstanceEnum( bstrClassName,
                                                 0,
                                                 NULL,
                                                 &pEnumDevices );

    if( FAILED( lastHR ) || pEnumDevices == NULL )
        goto ExitSetupXinput;

    // Check all X input devices
    while( true )
    {
        lastHR = pEnumDevices->Next( 10000, MAX_XINPUT_DEVICES, pDevices, &uReturned );

        if( FAILED( lastHR ) )
            break;

        if( uReturned == 0 )
            break;

        for( iDevice = 0; iDevice<uReturned; iDevice++ )
        {
            if ( pDevices[iDevice] == NULL )
               continue;

            // For each device, get its device ID
            lastHR = pDevices[iDevice]->Get( bstrDeviceID,
                                             0L,
                                             &var,
                                             NULL,
                                             NULL );

            if ( SUCCEEDED( lastHR ) && ( var.vt == VT_BSTR ) && ( var.bstrVal != NULL ) )
            {
                if( _tcsstr( var.bstrVal, TEXT("IG_" ) ) != NULL )
                {
                    // If it does, then get the VID/PID from var.bstrVal
                    DWORD dwPid = 0;
                    DWORD dwVid = 0;

                    TCHAR* strVid = _tcsstr( var.bstrVal, TEXT("VID_") );
                    if( strVid && _stscanf( strVid, TEXT("VID_%4X"), &dwVid ) != 1 )
                        dwVid = 0;

                    TCHAR* strPid = _tcsstr( var.bstrVal, TEXT("PID_" ) );
                    if( strPid && _stscanf( strPid, TEXT("PID_%4X"), &dwPid ) != 1 )
                        dwPid = 0;

                    DWORD dwVidPid = MAKELONG( dwVid, dwPid );

                    // Add the VID/PID to a linked list
                    XINPUT_DEVICE_NODE* pNewNode = new XINPUT_DEVICE_NODE;
                    if( pNewNode != NULL )
                    {
                        pNewNode->dwVidPid = dwVidPid;
                        pNewNode->pNext    = pDxInpXmodeDevList;
                        pDxInpXmodeDevList = pNewNode;
                    }
                }
            }

            SafeRelease( &pDevices[iDevice] );
        }

    }

ExitSetupXinput:
    if( bstrNamespace != NULL )
        SysFreeString( bstrNamespace );

    if( bstrDeviceID != NULL )
        SysFreeString( bstrDeviceID );

    if( bstrClassName != NULL )
        SysFreeString( bstrClassName );

    for( iDevice = 0; iDevice < MAX_XINPUT_DEVICES; iDevice++ )
    {
        if ( pDevices[iDevice] != NULL )
        {
            SafeRelease( &pDevices[iDevice] );
        }
    }

    if ( pEnumDevices != NULL )
    {
        SafeRelease( &pEnumDevices );
    }

    if ( pIWbemLocator != NULL )
    {
        SafeRelease( &pIWbemLocator );
    }

    if ( pIWbemServices != NULL )
    {
        SafeRelease( &pIWbemServices );
    }

    return lastHR;
}

bool IsXInputDevice( const GUID* pGuidProductFromDirectInput )
{
    XINPUT_DEVICE_NODE* pNode = pDxInpXmodeDevList;

    while( pNode != NULL )
    {
        if( pNode->dwVidPid == pGuidProductFromDirectInput->Data1 )
            return true;

        pNode = pNode->pNext;
    }

    return false;
}

void CleanupXInputDevice()
{
    // Cleanup linked list
    XINPUT_DEVICE_NODE* pNode = pDxInpXmodeDevList;
    while( pNode )
    {
        XINPUT_DEVICE_NODE* pDelete = pNode;
        pNode = pNode->pNext;
        SAFE_DELETE( pDelete );
    }
}

BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,
                                     VOID* pContext )
{
    DI_ENUM_CONTEXT* pEnumContext = reinterpret_cast<DI_ENUM_CONTEXT*>( pContext );
    HRESULT          hr;

    if( isDxInpXmode && IsXInputDevice( &pdidInstance->guidProduct ) )
        return DIENUM_CONTINUE;

    // Skip anything other than the perferred joystick device as defined by the control panel.
    // Instead you could store all the enumerated joysticks and let the user pick.
    if( pEnumContext->bPreferredJoyCfgValid &&
        !IsEqualGUID( pdidInstance->guidInstance, pEnumContext->pPreferredJoyCfg->guidInstance ) )
        return DIENUM_CONTINUE;

    // Obtain an interface to the enumerated joystick.
    hr = pDxInput->CreateDevice( pdidInstance->guidInstance, &pDxInpJoy, NULL );

    if( FAILED( hr ) )
        return DIENUM_CONTINUE;

    if( pEnumContext->bPreferredJoyCfgValid &&
        !IsEqualGUID( pdidInstance->guidInstance, pEnumContext->pPreferredJoyCfg->guidInstance ) )
        return DIENUM_CONTINUE;

    return DIENUM_STOP;
}


//-----------------------------------------------------------------------------
// Name: EnumObjectsCallback()
// Desc: Callback function for enumerating objects (axes, buttons, POVs) on a
//       joystick. This function enables user interface elements for objects
//       that are found to exist, and scales axes min/max values.
//-----------------------------------------------------------------------------
BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext )
{

    if( pdidoi->dwType & DIDFT_AXIS )
    {
        DIPROPRANGE diprg;
        diprg.diph.dwSize = sizeof( DIPROPRANGE );
        diprg.diph.dwHeaderSize = sizeof( DIPROPHEADER );
        diprg.diph.dwHow = DIPH_BYID;
        diprg.diph.dwObj = pdidoi->dwType; // Specify the enumerated axis
        diprg.lMin = -1000;
        diprg.lMax = +1000;

        // Set the range for the axis
        if( FAILED( pDxInpJoy->SetProperty( DIPROP_RANGE, &diprg.diph ) ) )
            return DIENUM_STOP;

    }

    // Set the UI to reflect what objects the joystick supports
    if ( pdidoi->guidType == GUID_XAxis )
    {
        DxJoyOption.Axis_X = true;
    }
    else
    if ( pdidoi->guidType == GUID_YAxis )
    {
        DxJoyOption.Axis_Y = true;
    }
    else
    if ( pdidoi->guidType == GUID_ZAxis )
    {
        DxJoyOption.Axis_Z = true;
    }
    else
    if ( pdidoi->guidType == GUID_RxAxis )
    {
        DxJoyOption.Axis_X_ROT = true;
    }
    else
    if ( pdidoi->guidType == GUID_RyAxis )
    {
        DxJoyOption.Axis_Y_ROT = true;
    }
    else
    if ( pdidoi->guidType == GUID_RzAxis )
    {
        DxJoyOption.Axis_Z_ROT = true;
    }
    else
    if ( pdidoi->guidType == GUID_Slider )
    {
        if ( DxJoyOption.Sliders < XJOYSTICK_MAX_SLIDERS )
        {
            DxJoyOption.Slider[ DxJoyOption.Sliders++ ] = true;
        }
    }
    else
    if ( pdidoi->guidType == GUID_POV )
    {
        if ( DxJoyOption.POVs < XJOYSTICK_MAX_POVS )
        {
            DxJoyOption.POV[ DxJoyOption.POVs++ ] = true;
        }
    }
    else
    if ( pdidoi->guidType == GUID_Button )
    {
        if ( DxJoyOption.Buttons < XJOYSTICK_MAX_BUTTONS )
        {
            DxJoyOption.Button[ DxJoyOption.Buttons++ ] = true;
        }
    }
    else
    if ( pdidoi->guidType == GUID_Key )
    {
        if ( DxJoyOption.Keys < XJOYSTICK_MAX_KEYS )
        {
            DxJoyOption.Key[ DxJoyOption.Keys ] = true;
        }
    }

    return DIENUM_CONTINUE;
}


HRESULT UpdateInputStateJoystick()
{
    HRESULT hr = S_FALSE;

    if( pDxInpJoy == NULL )
        return hr;

    hr = pDxInpJoy->Poll();
    if( FAILED( hr ) )
    {
        hr = pDxInpJoy->Acquire();

        while( hr == DIERR_INPUTLOST )
        {
            hr = pDxInpJoy->Acquire();
        }

        return S_OK;
    }

    // Get the input's device state
    if ( FAILED( hr = pDxInpJoy->GetDeviceState( sizeof( DIJOYSTATE2 ), &DxJoyState ) ) )
        return hr; // The device should have been acquired during the Poll()

    return S_OK;
}

HRESULT UpdateInputStateKeyboard()
{
    HRESULT hr = S_FALSE;

    if ( pDxInpKbd == NULL )
        return hr;

    while( hr = pDxInpKbd->GetDeviceState( 256, &DxKbdState ) == DIERR_INPUTLOST )
    {
        if ( FAILED( hr = pDxInpKbd->Acquire() ) )
        {
            return hr;
        }
    }

    return S_OK;
}

HRESULT UpdateInputStateMouse()
{
    HRESULT hr = S_FALSE;

    if ( pDxInpMse == NULL )
    {
        return hr;
    }

    hr = pDxInpMse->GetDeviceState( sizeof(DIMOUSESTATE2), &DxMseState );

    if ( FAILED( hr ) )
    {
        hr = pDxInpMse->Acquire();
        while( hr == DIERR_INPUTLOST )
        {
            hr = pDxInpMse->Acquire();
        }

        return S_OK;
    }

    return S_OK;
}

void GetJoystickOptions( JOYSTICK_OPTIONS* jopt )
{
    if ( ( pDxInpJoy != NULL ) && ( pDxInput != NULL ) )
    {
        memcpy( jopt, &DxJoyOption, sizeof( JOYSTICK_OPTIONS ) );
    }
}

void GetJoystickState( JOYSTATE* jst )
{
    if ( ( pDxInpJoy != NULL ) && ( pDxInput != NULL ) )
    {
        memcpy( jst, &DxJoyState, sizeof( JOYSTATE ) );
    }
}

void GetKeyboardState( KEYSTATE* kst )
{
    if ( ( pDxInpKbd != NULL ) && ( pDxInput != NULL ) )
    {
        kst->length = 0;
        memset( kst->keys, 0, 256 );

        for( int cnt=0; cnt<256; cnt ++ )
        {
            if ( ( DxKbdState[cnt] & 0x80 ) > 0 )
            {
                kst->keys[ kst->length ] = cnt;
                kst->length++;
            }
        }
    }
}

void GetMouseState( MOUSESTATE* mst )
{
    if ( ( pDxInpMse != NULL ) && ( pDxInput != NULL ) )
    {
        memcpy( mst, &DxMseState, sizeof( MOUSESTATE ) );
    }
}

VOID FreeDirectInput()
{
    if ( pDxInpJoy != NULL )
    {
        pDxInpJoy->Unacquire();
    }

    if ( pDxInpMse != NULL )
    {
        pDxInpMse->Unacquire();
    }

    if ( pDxInpKbd != NULL )
    {
        pDxInpKbd->Unacquire();
    }

    // Release any DirectInput objects.
    SafeRelease( &pDxInpJoy );
    SafeRelease( &pDxInpMse );
    SafeRelease( &pDxInpKbd );
    SafeRelease( &pDxInput );
}

HRESULT GetDXInputLastResult()
{
    return lastHR;
}
