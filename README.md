# winjoysticktest

An open source program for testing Windows Joystick

## Works with

* Windows7, 8, 10
* 32, 64 bit

## Requirements 

* MSYS2
* fltk-1.3.4-2-ts : https://github.com/rageworx/fltk-1.3.4-2-ts
* fl_imgtk : https://github.com/rageworx/fl_imgtk

### fltk-1.3.4-2-ts

Need to configure with configMinGW.sh in MSYS2.

### fl_imgtk

Build with Makefile.gcc
    
### To do before build

* Build fltk-1.3.4-2-ts and install to MSYS2
* Build fl_imgtk and copy all files in fltk_imgtk/lib to lib directory of this project.