# Makefile for Make WinJoystickTest for MinGW-W64 + M-SYS
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar
WRC = windres

# FLTK configs 
FCG          = fltk-config --use-images
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# Base PATH
BASE_PATH = .
SRC_PATH  = $(BASE_PATH)/src
FSRC_PATH = $(SRC_PATH)/fl

# Where fl_imgtk ?
LIBFLIMGTK = ../fl_imgtk/lib

# TARGET settings
TARGET_PKG = winjoysticktester
TARGET_DIR = ./bin
TARGET_OBJ = ./obj
LIBWIN32S  = -lole32 -luuid -lcomctl32 -lwsock32 -lm 
LIBWIN32S += -lgdi32 -luser32 -lkernel32 -lShlwapi -lcomdlg32
LIBWIN32S += -loleaut32 -ldinput -ldinput8 -ldxguid

# DEFINITIONS
DEFS  = -D_POSIX_THREADS -DSUPPORT_DRAGDROP -DMININI_ANSI
DEFS += -DWIN32 -DUNICODE -D_UNICODE -DSUPPORT_WCHAR 
DEFS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT
DEFS += -DUSE_OMP -D_WIN32

# Compiler optiops 
COPTS  = -mms-bitfields -mwindows 
COPTS += -ffast-math -fexceptions -fopenmp -O2 -s

# CC FLAG
CFLAGS  = -I$(SRC_PATH) -I$(FSRC_PATH)
CFLAGS += -I$(LIBFLIMGTK)
CFLAGS += -Ires
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += $(DEFS)
WFLGAS  = $(CFLAGS)
CFLAGS += $(COPTS)

# LINK FLAG
LFLAGS  =
LFLAGS += -L$(LIBFLIMGTK)
LFLAGS += -static
LFLAGS += $(LIBWIN32S)
LFLAGS += -lfl_imgtk
LFLAGS += $(FLTKCFG_LFG)
LFLAGS += -lpthread

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare continue packaging

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

packaging:
	@cp -rf LICENSE ${TARGET_DIR}
	@cp -rf readme.md ${TARGET_DIR}

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(WROBJ): $(WRES)
	@echo "Building windows resource ..."
	@$(WRC) -i $(WRES) $(WFLAGS) -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(WROBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
